# README

tmux makes it easy to set up a working environment for a project, but it's not
always easy to recreate the environment when you need it.  With `tmux-session`,
you can create a configuration file which contains all the tmux commands
necessary for setting up the environment.  If the session has already been
started, it will simply reattach to the session.

I've included an example configuration which showcases how I use this tool
within a django project. The example shows how to start virtual environments
and run commands on starting the session.  Also included is a completion file
for zsh.  If you understand what that means, you likely already know where it
needs to be to work.
